# User Management Application

Simple user management application built with React + Hapi.js.

## Prerequisites
    - MongoDB
    https://treehouse.github.io/installation-guides/mac/mongo-mac.html MAC
    https://docs.mongodb.com/v3.2/tutorial/install-mongodb-on-windows Windows
## Development

### Steps
1. **Run your MongoDb daemon**
2. **Configure API config**
3. **Start the API by running app.js** (/user-management-app-api** )
3. **Start the React application by using npm start**

### Configurable values
### API
#### Default values for the API are available under config/default.json

However, You do need to configure the following:

```
"apiUrl": "http://localhost:3001",
"emailService": {
    "enabled": false,
    "user": "example@example.com",
    "apiKey": "api_key_from_sendgrid.com"
}
```

- apiUrl - where the application is running (requiered for sending emails for validation, resetting password etc.)
- emailService.apiKey - apiKey from https://sendgrid.com/. Create a free trial account and get Your apiKey
Set emailService.enabled to true if You want to actually send out emails

### API Swagger documentation can be found under API_URL/documentation
### Example: http://localhost:3001/documentation


### Front-end

By default, the front-end app. assumes that the API is running on http://localhost:3001

This can be configured via the environment variable named **REACT_APP_API_URL**

Please do this by creating an .env file inside **user-manager-app-www** with content like:
```REACT_APP_API_URL=http://localhost:3001```

### Tests

Currently there is only a set of intergration tests for the API.
NB! I am not using an in memory database for the tests, so make sure that You are running the tests against the correct DB.

To run the tests:

- Run the API (with a testDabase (check config) ``` "mongodb": {
        "uri": "mongodb://localhost/testDatabase"
    }```
- Use the command **npm test** that will run the test

