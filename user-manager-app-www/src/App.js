import React, { Component } from 'react';
import RouterComponent from './router/router'

export default class App extends Component {
    render () {
        return (
            <RouterComponent />
        );
    }
}
