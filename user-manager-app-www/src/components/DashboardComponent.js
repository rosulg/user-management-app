import React, { Component } from 'react';
import UserListComponent from './UserListComponent'
import UserDetailViewComponent from './UserDetailViewComponent'
import AddUserComponent from './AddUserComponent'
import {
    Nav,
    NavItem,
    Navbar
} from 'react-bootstrap';

export default class DashboardComponent extends Component {
    constructor (props) {
        super(props);
        this.state = {
            contentView: 'userList',
            windowWidth: window.innerWidth
        };
    }

    componentWillMount () {
        window.addEventListener('resize', this.handleWindowSizeChange);
    }

    componentWillUnmount () {
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ windowWidth: window.innerWidth });
    };

    logout () {
        localStorage.removeItem('login');
        window.location = '/';
    }

    renderContentView () {
        const content = this.state.contentView;
        switch (content) {
        case 'userList':
            return (
                <UserListComponent
                    viewUserDetails={this.viewUserDetails.bind(this)}
                />
            );
        case 'userDetail':
            return <UserDetailViewComponent userInfo={this.state.userInfo} />
        case 'addUser':
            return <AddUserComponent />
        default:
            return null;
        }
    }

    viewUserDetails (userInfo) {
        this.setState({contentView: 'userDetail', userInfo})
    }

    handleNavbarSelect (selectedView) {
        this.setState({contentView: selectedView})
    }

    render () {
        const width = this.state.windowWidth;
        const isMobile = width <= 500;

        return (
            <div>
                <Navbar
                    style={styles.navBar}
                    onSelect={this.handleNavbarSelect.bind(this)}
                >
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href='/' onClick={() => this.logout()}>Log out</a>
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Nav>
                        <NavItem eventKey={'userList'}>
                                View users
                        </NavItem>
                        <NavItem eventKey={'addUser'}>
                                Add user
                        </NavItem>
                    </Nav>
                </Navbar>
                <div style={styles.contentViewContainer}>
                    <div style={styles.childContainer(isMobile)}>
                        {this.renderContentView()}
                    </div>

                </div>
            </div>
        );
    }
}

const styles = {
    childContainer: (isMobile) => ({
        width: isMobile ? '100%' : '40%',
        minWidth: isMobile ? '0px' : '500px'
    }),
    navBar: {
        color: 'white',
        fontSize: '18px',
        fontWeight: 'bold',
        backgroundColor: '#FBFCFC'
    },
    contentViewContainer: {
        padding: '40px',
        flexDirection: 'column',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}
