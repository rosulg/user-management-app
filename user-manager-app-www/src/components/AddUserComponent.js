import React, { Component } from 'react';
import FormComponent from './common/FormComponent';
import Api from '../lib/api';
import '../styles/App.css';

export default class AddUserComponent extends Component {
    constructor (props) {
        super(props);
        this.state = {
            msg: null
        }
    }

    async addUser (payload) {
        const user = await Api.createUser(payload);
        if (user) {
            this.setState({msg: 'User created!'})
        }
    }

    render () {
        return (
            <div className='form-container'>
                <h1> Add new user </h1>
                <p>
                    {
                        this.state.msg ?
                            this.state.msg
                            :
                            `Please fill in the details for the new user.
                            A confirmation link will be sent to the entered email.`
                    }
                </p>
                <div className='form-fields'>
                    <FormComponent
                        type={'addNewUser'}
                        buttonName={'Add new user'}
                        heading={'Add a new user'}
                        doOnSubmit={(payload) => this.addUser(payload)}
                    />
                </div>
            </div>
        );
    }
}