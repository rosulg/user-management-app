import React, { Component } from 'react';
import FormComponent from './common/FormComponent';
import { Link } from 'react-router-dom';

import Api from '../lib/api';

import '../styles/App.css';

export default class PasswordRecoveryComponent extends Component {
    async requestNewPassword (payload) {
        const response = await Api.requestNewPassword(payload)

        return response;
    }

    render () {
        return (
            <div className='form-container' style={{height: '100vh'}}>
                <h1> Recover Your password </h1>
                <p>
                    Please provide us with an email.
                    Instruction how to recover your password will be
                    sent to the user's email
                </p>
                <Link to="/">
                    <h2>Back to home</h2>
                </Link>
                <div className='form-fields' >
                    <FormComponent
                        type={'passwordRecovery'}
                        buttonName={'Recover account'}
                        heading={"Enter your account's email"}
                        fields={['email']}
                        doOnSubmit={(payload) => this.requestNewPassword(payload)}
                    />
                </div>
            </div>
        );
    }
}