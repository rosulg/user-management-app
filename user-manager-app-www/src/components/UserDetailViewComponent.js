import React, { Component } from 'react';
import {
    ListGroup,
    ListGroupItem
} from 'react-bootstrap';

import Api from '../lib/api';
import profileImg from '../assets/img/logo.svg';

export default class UserDetailViewComponent extends Component {
    constructor (props) {
        super(props);
        this.state = {
            userLogins: []
        }
    }

    async componentWillMount () {
        try {
            const userId = this.props.userInfo._id;
            const response = await Api.getUserActions(userId, 'login');
            const userLogins = response.map(login => login.createdAt);
            this.setState({userLogins})
        }
        catch (err) {
            this.setState({userLogins: ['Something went wrong.']})
        }
    }

    renderLoginsListItems () {
        return this.state.userLogins.map((login, index) =>
            <ListGroupItem key={index}>{JSON.stringify(login)}</ListGroupItem>
        )
    }

    render () {
        return (
            <div style={styles.userDetailViewContainer}>
                <div style={styles.userInfoContainer}>
                    <h3>{this.props.userInfo.email || 'Unknown'}</h3>
                    <img
                        src={profileImg}
                        className='App-logo'
                        alt='user profile img'
                        style={styles.profileImg}
                    />
                </div>
                <div style={styles.loginInfoContainer}>
                    <h3>Login events</h3>
                    <div>
                        <ListGroup>
                            {this.renderLoginsListItems()}
                        </ListGroup>
                    </div>
                </div>
            </div>
        );
    }
}

const styles = {
    userDetailViewContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'center',
        flexWrap: 'wrap'
    },
    profileImg: {
        width: '150px',
        height: '150px'
    },
    userInfoContainer: {
        padding: '20px',
        flex: '1 0'
    },
    loginInfoContainer: {
        padding: '20px',
        flex: '1 1',
        flexDirection: 'column'
    }
}
