import React, { Component } from 'react';
import { Link } from 'react-router-dom';
export default class Notfound extends Component{
    render () {
        return (
            <div style={styles.notFound}>
                <div className="jumbotron" style={styles.jumbotron}>
                    <h1>No such route</h1>
                    <Link to="/">
                        <h2 style={{color: 'white'}}>Back to home</h2>
                    </Link>
                </div>
            </div>
        )
    }
}


const styles = {
    notFound: {
        textAlign: 'center',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    jumbotron: {
        minHeight: '600px',
        minWidth: '50vw',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#282c34',
        color: 'white'
    }
}