import React from 'react';
import {
    ControlLabel,
    FormGroup,
    FormControl,
    HelpBlock
} from 'react-bootstrap';

export default function FieldGroup ({ id, label, help, validationState, ...props }) {
    return (
        <FormGroup
            controlId={id}
            validationState={validationState}
        >
            <ControlLabel>{label}</ControlLabel>
            <FormControl {...props} />
            {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
    );
}