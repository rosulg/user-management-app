import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import FieldGroup from './helpers/FieldGroup'

export default class FormComponent extends Component {
    constructor (props) {
        super(props);

        this.state = {
            password: '',
            passwordHelp: this.props.passwordHelp,
            email: '',
            emailHelp: ''
        }

        this.initialState = this.state;
    }

    onFormChange (key, value) {
        this.setState({[key]: value.target.value});
    }

    getPasswordValidationState () {
        const length = this.state.password.length;
        if (length >= 8) return 'success';
        else if (length > 5) return 'warning';
        else if (length > 0) return 'error';
        return null;
    }

    getEmailValidationState () {
        const length = this.state.email.length;
        if (length > 3) {
            return this.state.email.includes('@') ? 'success' : 'warning';
        }
        return null;
    }

    renderFormFields () {
        const fieldsWanted = this.props.fields;
        const fields = [
            {
                name: 'email',
                element: (
                    <FieldGroup
                        key={1}
                        type='email'
                        label='Email address'
                        autoComplete='off'
                        validationState={this.getEmailValidationState()}
                        placeholder='Enter email'
                        value={this.state.email}
                        onChange={(value) => this.onFormChange('email', value)}
                        autoFocus
                        required
                    />
                )
            },
            {
                name: 'password',
                element: (
                    <FieldGroup
                        key={2}
                        label='Password'
                        autoComplete='off'
                        type='password'
                        validationState={this.getPasswordValidationState()}
                        help={this.state.passwordHelp}
                        placeholder='password'
                        value={this.state.password}
                        onChange={(value) => this.onFormChange('password', value)}
                        pattern='.{8,}'
                        required
                    />
                )
            }
        ]
        if (fieldsWanted) {
            return fields.map((el) => {
                if (fieldsWanted.includes(el.name)) {
                    return el.element;
                }
            })
        }
        return fields.map(el => el.element);
    }

    renderPassWordRecoveryLink () {

        if (this.props.passwordRecoveryLink) {
            return (
                <div style={{textAlign: 'right'}}>
                    <Link to='/password_recovery'>
                       Recover password
                    </Link>
                </div>
            )
        }
    }

    renderHeading () {
        if (this.state.error) {
            return (
                <div className='alert alert-danger'>
                    { this.state.error }
                </div>
            )
        }
        return (
            <div className='alert alert-info'>
                {
                    this.props.heading || 'Greetings'
                }
            </div>
        )
    }

    handleErrorMessages (statusCode) {
        switch(statusCode) {
        case 401:
            return 'Wrong username/password combination';
        case 409:
            return 'Conflict. User already exists';
        default:
            return 'Something went wrong';
        }
    }

    async handleSubmit () {
        if (this.props.doOnSubmit) {
            try {
                await this.props.doOnSubmit({
                    email: this.state.email || undefined,
                    password: this.state.password || undefined
                });
            }
            catch (err) {
                this.setState({error: this.handleErrorMessages(err.error.statusCode)});
            }
        }
        this.setState(this.initialState)
        return null;
    }

    render () {
        return (
            <div>
                {this.renderHeading()}
                {this.renderPassWordRecoveryLink()}
                <form onSubmit={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    this.handleSubmit();
                }}
                action='#'>

                    {this.renderFormFields()}

                    <Button bsStyle='success' type='submit'>
                        {
                            this.props.buttonName || 'Click'
                        }
                    </Button>
                </form>
            </div>

        );
    }
}