import React, { Component } from 'react';
import {
    ListGroup,
    ListGroupItem,
    Button,
    Glyphicon,
    Pager
} from 'react-bootstrap';

import Api from '../lib/api';

export default class UserListComponent extends Component {
    constructor (props) {
        super(props);
        this.state = {
            contentView: null,
            windowWidth: window.innerWidth,
            users: [],
            page: 0
        };
    }

    async loadUsers (page) {
        const users = await Api.getUsers(page);
        this.setState({users});
    }

    async componentWillMount () {
        await this.loadUsers(this.state.page);
        window.addEventListener('resize', this.handleWindowSizeChange);
    }

    componentWillUnmount () {
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };

    async deleteUser (id) {
        const confirmation = window.confirm('Are you sure you want to delete this user?')
        if (confirmation) {
            await Api.deleteUser(id);
            await this.loadUsers(this.state.page);
        }
    }

    renderUserListItems () {
        return this.state.users.map((user, index) => (
            <ListGroupItem
                className='userlist-item'
                key={index}
                header={user.name}
                style={styles.listGroupItem}
            >
                <Button
                    style={styles.deleteUserButton}
                    onClick={() => this.deleteUser(user._id)}
                >
                    <Glyphicon style={styles.listGlyphicon} glyph='remove' />
                </Button>
                {user.email} <br />
                <Button
                    bsStyle='success'
                    onClick={() => this.props.viewUserDetails(user)}
                    style={styles.viewUserDetailsButton}
                >
                    View profile
                </Button>
                <p style={styles.validated(user.validated)}> validated </p>
            </ListGroupItem>
        ))

    }

    handleNavbarSelect (selectedView) {
        this.setState({contentView: selectedView})
    }

    async paginate (action) {
        let page = this.state.page;
        if (action === 'next') {
            page = page + 1;
        }
        else {
            const previous = page - 1;
            if (previous >= 0) {
                page = previous;
            }
        };
        this.loadUsers(page);
        this.setState({page})
    }

    render () {
        return (
            <div style={{textAlign: 'center'}}>
                <Pager>
                    <p>Page: { this.state.page + 1 }</p>
                    <Pager.Item style={{marginRight: '5px'}} onClick={() => this.paginate('previous')}>Previous</Pager.Item>
                    <Pager.Item style={{marginLeft: '5px'}} onClick={() => this.paginate('next')}>Next</Pager.Item>
                </Pager>
                {
                    this.state.users.length > 0 ?
                        (<ListGroup>{this.renderUserListItems()}</ListGroup>) :
                        (<span style={{fontSize: '24px', fontWeight: 'bold'}}>No users to show</span>)
                }
            </div>
        );
    }
}

const styles = {
    validated: (validated) => ({
        color: validated ? 'green' : 'red',
        position: 'absolute',
        bottom: '0px',
        right: '5px'
    }),
    listGroupItem: {
        marginTop: '10px',
        marginBottom: '10px',
        textAlign: 'left'
    },
    deleteUserButton: {
        position: 'absolute',
        top: '5px',
        right: '5px',
        margin: '5px'
    },
    viewUserDetailsButton: {
        marginTop: '10px'
    },
    listGlyphicon: {
        color: 'red'
    }
}
