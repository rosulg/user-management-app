import React, { Component } from 'react';
import FormComponent from './common/FormComponent';
import { Tab, Tabs } from 'react-bootstrap';

import logo from '../assets/img/logo.svg';
import '../styles/App.css';
import Api from '../lib/api';

export default class LandingPageComponent extends Component {

    async login (payload) {
        const response = await Api.login(payload)

        if (response && response.token) {
            window.location ='/dashboard';
        }
        return response;
    }

    async register (payload) {
        const response = await Api.register(payload)

        if (response && response.token) {
            window.location ='/dashboard';
        }
        return response;
    }

    render () {
        return (
            <div className='App'>
                <header className='App-header'>
                    <img src={logo} className='App-logo' alt='logo' />
                    <h1> User management application </h1>
                    <h4> by Robin Sulg </h4>
                </header>
                <div className='form-container'>
                    <Tabs defaultActiveKey={1} id='tabs'>
                        <Tab eventKey={1} title='login'>
                            <div className='form-fields'>
                                <FormComponent
                                    type={'login'}
                                    passwordRecoveryLink={true}
                                    passwordHelp='Password must be atleast 8 characters long'
                                    buttonName={'Sign in'}
                                    heading={'Enter your email and password and sign in'}
                                    doOnSubmit={(payload) => this.login(payload)}
                                />
                            </div>
                        </Tab>
                        <Tab eventKey={2} title='Register'>
                            <div className='form-fields'>
                                <FormComponent
                                    type={'register'}
                                    doOnSubmit={(payload) => this.register(payload)}
                                    buttonName={'Register'}
                                    heading={'Register'}
                                />
                            </div>
                        </Tab>
                    </Tabs>
                </div>
            </div>
        );
    }
}