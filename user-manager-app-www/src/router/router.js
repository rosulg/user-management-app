import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import LandingPageCompnent from '../components/LandingPageComponent';
import PasswordRecoveryComponent from '../components/PasswordRecoveryComponent';
import DashboardComponent from '../components/DashboardComponent';
import NotFound from '../components/NotFound';
import PrivateRoute from './PrivateRoute';

export default class RouterComponent extends Component {
    render () {
        return (
            <Router>
                <Switch>
                    <Route path='/' exact component={LandingPageCompnent} />
                    <PrivateRoute authed={true}
                        path='/dashboard' component={DashboardComponent}
                    />
                    <Route
                        path='/password_recovery'
                        component={PasswordRecoveryComponent}
                    />
                    <Route component={NotFound} />
                </Switch>
            </Router>
        );
    }
}
