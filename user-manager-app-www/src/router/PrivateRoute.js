import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

function PrivateRoute ({component: Component, authed, ...rest}) {
    const login = window.localStorage.getItem('login');
    return (
        <Route
            {...rest}
            render={(props) => login ?
                (
                    <div>
                        <Component {...props} />
                    </div>
                )
                :
                <Redirect
                    to={{
                        pathname: '/',
                        state: {from: props.location}}}
                />}
        />
    )
}

PrivateRoute.propTypes = {
    location: PropTypes.object,
    component: PropTypes.func,
    authed: PropTypes.bool
};

export default PrivateRoute;
