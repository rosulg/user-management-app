const rp = require('request-promise');

const API_URL = process.env.REACT_APP_API_URL ? process.env.REACT_APP_API_URL : `http://localhost:3001`;

/*
    Note!
    I am assuming that this appilication will be used over HTTPS.
    That is why I am not hashing sensitive information like password
    when making requests.

    If it would come to be that the application will be only used over HTTP.
    Then I would even consider encrypting the payload.

    If this application is used with HTTP it IS vurnable to man in the middle attacks
*/
export default class Api {

    static async apiCall (options) {
        const token = localStorage.getItem('login');

        options.headers = Object.assign(
            {},
            options.header,
            { Authorization: token }
        );

        options.json = true;
        const response = await rp(options);
        return response;
    }

    static async login (payload) {

        const response = await rp({
            method: 'POST',
            uri: `${API_URL}/login`,
            body: payload,
            json: true
        });
        if (response && response.token) {
            window.localStorage.setItem('login', response.token);
        }
        return response;
    }

    static async register (payload) {

        const response = await rp({
            method: 'POST',
            uri: `${API_URL}/register`,
            body: payload,
            json: true
        });
        if (response && response.token) {
            window.localStorage.setItem('login', response.token);
        }
        return response;
    }

    static async requestNewPassword (payload) {

        const response = await rp({
            method: 'POST',
            uri: `${API_URL}/requestNewPassword`,
            body: payload,
            json: true
        });

        return response;
    }

    static createUser (payload) {
        return Api.apiCall({
            method: 'POST',
            uri: `${API_URL}/user`,
            body: payload
        })
    }

    static getUsers (pagination) {
        return Api.apiCall({
            method: 'GET',
            uri: `${API_URL}/users${pagination !== null && pagination !== undefined ? `?page=${pagination}` : ''}`
        })
    }

    static getUser (id) {
        return Api.apiCall({
            uri: `${API_URL}/users/${id}`,
            method: 'GET'
        })
    }

    static getUserActions (userId, actionType) {
        return Api.apiCall({
            uri: `${API_URL}/userAction/${userId}/${actionType}`,
            method: 'GET'
        })
    }

    static updateUser (id, user) {
        return Api.apiCall({
            method: 'PUT',
            body: user,
            uri: `${API_URL}/users/${id}`
        });
    }

    static deleteUser (id) {
        return Api.apiCall({
            method: 'DELETE',
            uri: `${API_URL}/user/${id}`
        })
    }

}