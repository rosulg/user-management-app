const Boom = require('boom');

const { UserActions } = Models;

module.exports = {
    async create (request, h) {
        try {
            const userAction = await new UserActions(request.payload).save();
            return h.response({
                success: 'user_action_created',
                userAction
            }).code(201);
        }
        catch (err) {
            throw Boom.badRequest(err);
        }
    },
    async deleteAllRecords (request) {
        try {
            await UserActions.deleteMany({ userId: request.params.id, action: request.params.actionType });
            return {
                success: 'user_actions_deleted'
            };
        }
        catch (err) {
            return Boom.badData(err);
        }
    },
    async findAll (request) {
        try {
            const userActions = await UserActions.find(
                { userId: request.params.userId, action: request.params.actionType }
            );

            if (userActions) {
                return userActions;
            }
            return Boom.notFound();
        }
        catch (err) {
            return Boom.badRequest();
        }
    },
};
