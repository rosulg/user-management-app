const Boom = require('boom');
const Config = require('config');

const { User } = Models;
const UserActionsController = require('./userActions.controller');
const AuthService = require('../services/auth.service.js');
const EmailService = require('../services/email.service.js');


module.exports = {

    async login (request, h) {
        try {
            request.payload.email = request.payload.email.toLowerCase();

            const foundUser = await this.findOneOrNone(
                request, h
            );

            if (!foundUser) {
                return Boom.unauthorized('No such user,');
            }

            const token = await AuthService.authenticate(foundUser, request.payload.password);

            if (token) {
                const req = {
                    payload: {
                        userId: foundUser._id,
                        action: 'login'
                    }
                };

                // Save login event
                await UserActionsController.create(req, h);

                return token;
            }
            return Boom.unauthorized();
        }
        catch (err) {
            console.log(err, 'error');
            return Boom.badRequest();
        }
    },

    async create (request, h) {
        try {
            request.payload.email = request.payload.email.toLowerCase();

            const doesUserExist = await this.findOneOrNone(
                request, h
            );

            if (doesUserExist) {
                return Boom.conflict('User with provided email already exists');
            }

            const createdUser = await new User(request.payload).save();

            if (createdUser) {
                const token = await AuthService.createToken(createdUser);

                const emailContent = {
                    email: createdUser.email,
                    validationLink: `${Config.get('server.apiUrl')}/validateUser/${Buffer.from(createdUser._id.toString()).toString('base64')}`
                };
                EmailService.sendEmail(emailContent, 'greetings_email_body.html');

                return h.response({
                    success: 'user_created',
                    createdUser: {
                        id: createdUser.id,
                        email: createdUser.email,
                        createdAt: createdUser.createdAt
                    },
                    token
                }).code(201);
            }
            return Boom.badRequest();
        }
        catch (err) {
            return Boom.badRequest(err);
        }
    },

    async remove (request) {
        try {
            await User.findOneAndRemove({
                _id: request.params.id
            });

            request.params.actionType = 'login';
            await UserActionsController.deleteAllRecords(request);

            return {
                success: 'user_delete'
            };
        }
        catch (err) {
            return Boom.badData(err);
        }
    },

    async find (request) {
        try {
            const query = await User.find({
                _id: request.params.id
            });
            return query;
        }
        catch (err) {
            return Boom.badData(err);
        }
    },

    async findOneOrNone (request) {
        try {
            const constraints = [];

            if (request.params.id) {
                constraints.push({ _id: request.params.id });
            }
            else if (request.payload.email) {
                constraints.push({ email: request.payload.email });
            }

            const query = await User.find({
                $or: constraints
            });

            return query && query.length ? query[0] : null;
        }
        catch (err) {
            return Boom.badData(err);
        }
    },

    async findAll (request) {
        try {
            let constraint = {};
            if (
                request.query.page !== null
                && request.query.page !== undefined
            ) {
                const perPage = 5;
                constraint = {
                    skip: perPage * request.query.page,
                    limit: perPage
                };
            }
            const query = await User.find({}, 'email validated', constraint);
            return query;
        }
        catch (err) {
            return Boom.badData(err);
        }
    },

    async validateUser (request) {
        try {
            const userId = Buffer.from(request.params.encodedUserId, 'base64').toString('ascii');
            request.params.id = userId;
            request.payload = {
                validated: true
            };

            await this.update(request);

            return {
                success: 'user_validated'
            };
        }
        catch (err) {
            return Boom.badRequest(err);
        }
    },

    async resetPassword (request) {
        try {
            const userId = Buffer.from(request.params.encodedUserId, 'base64').toString('ascii');

            request.params.id = userId;

            const user = await this.findOneOrNone(request);

            const newPassword = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

            request.payload = {
                password: newPassword
            };

            await this.update(request);

            const emailContent = {
                email: user.email,
                newPassword
            };

            await EmailService.sendEmail(emailContent, 'new_password_email_body.html');

            return {
                success: 'password_changed'
            };
        }
        catch (err) {
            return Boom.badRequest(err);
        }
    },

    async requestNewPassword (request) {
        try {
            request.payload.email = request.payload.email.toLowerCase();

            const user = await this.findOneOrNone(request);

            const emailContent = {
                email: user.email,
                newPasswordLink: `${Config.get('server.apiUrl')}/resetPassword/${Buffer.from(user._id.toString()).toString('base64')}`
            };

            await EmailService.sendEmail(emailContent, 'recover_password_email_body.html');

            return {
                success: 'confirmation_email_sent'
            };
        }
        catch (err) {
            return Boom.badRequest(err);
        }
    },

    async update (request) {
        try {
            const result = await User.findOneAndUpdate({
                _id: request.params.id
            }, request.payload, {
                new: true
            });
            return result;
        }
        catch (err) {
            return Boom.badRequest(err);
        }
    }
};
