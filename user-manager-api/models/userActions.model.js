module.exports = {
    schema: {
        createdAt: {
            type: Date,
            default: Date.now()
        },
        updatedAt: {
            type: Date,
            default: Date.now()
        },
        action: {
            type: String
        },
        userId: {
            type: String
        }
    },
    statics: {},
    methods: {},
    onSchema: {
        pre: {},
        post: {}
    }
};
