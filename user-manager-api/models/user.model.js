
const HashPassword = require('../services/hashPassword.service');

module.exports = {
    schema: {
        createdAt: {
            type: Date,
            default: Date.now()
        },
        updatedAt: {
            type: Date,
            default: Date.now()
        },
        validated: {
            type: Boolean,
            default: false
        },
        password: {
            type: String,
            required: [true, "can't be blank"]
        },
        email: {
            type: String,
            lowercase: true,
            unique: true,
            index: true,
            validate: {
                isAsync: true,
                async validator (value) {
                    try {
                        const query = await this.constructor.findOne({ email: value });

                        if (query) {
                            if (this.id === query.id) {
                                return true;
                            }
                            return false;
                        }

                        return true;
                    }
                    catch (err) {
                        throw err;
                    }
                },
                message: 'The email address is already taken!'
            },
            required: [true, "can't be blank"],
            match: [/\S+@\S+\.\S+/, 'is invalid']

        }
    },
    statics: {},
    methods: {},
    onSchema: {
        pre: {
            save: [
                HashPassword
            ],
            findOneAndUpdate: [
                HashPassword,
                function () {
                    this.update({}, {
                        $set: {
                            updatedAt: new Date()
                        }
                    });
                }
            ]
        },
        post: {}
    }
};
