
const { User } = Models;
const AuthService = require('../services/auth.service.js');


module.exports = async (decoded, request) => {
    try {
        decoded = await AuthService.verifyToken(request.headers.authorization);
        if (typeof decoded === 'undefined') {
            return {
                isValid: false
            };
        }

        const currentUser = await User.findOne({
            _id: decoded.id
        });

        if (!currentUser) {
            return {
                isValid: false
            };
        }

        request.currentUser = currentUser;

        return {
            isValid: true
        };
    }
    catch (err) {
        return {
            isValid: false
        };
    }
};
