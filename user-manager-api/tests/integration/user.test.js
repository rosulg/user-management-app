const Config = require('config');
const rp = require('request-promise');

const API_URL = Config.get('server.apiUrl');

/*
    For those tests to pass you need to have the API running
*/

describe('User happy path tests', () => {
    let jwtToken;
    let userId;

    const userCreds = {
        email: 'testman@gmail.com',
        password: 'password'
    };

    it('Should register/add a user and return a JWT', async () => {
        const response = await rp({
            method: 'POST',
            uri: `${API_URL}/register`,
            body: userCreds,
            json: true
        });

        expect(response).toHaveProperty('token');
        expect(response.token).toBeString();
        expect(response.createdUser).toBeTruthy();
        userId = response.createdUser.id;
        return response;
    });

    it('Should login in with the registered user', async () => {
        const response = await rp({
            method: 'POST',
            uri: `${API_URL}/login`,
            body: userCreds,
            json: true
        });

        expect(response).toHaveProperty('token');
        expect(response.token).toBeString();

        jwtToken = response.token;
    });


    it('Should return list of users', async () => {
        const response = await rp({
            method: 'GET',
            headers: { Authorization: jwtToken },
            uri: `${API_URL}/users`,
            json: true
        });

        expect(response).toBeArray();
        const user = response[0];
        expect(user).toContainKeys(['_id', 'email', 'validated']);
        expect(user._id).toBeString();
        expect(user.validated).toBeBoolean();
        expect(user.email).toBeString();
    });

    it('Should delete a user', async () => {
        const response = await rp({
            method: 'DELETE',
            headers: { Authorization: jwtToken },
            uri: `${API_URL}/user/${userId}`,
            json: true
        });

        expect(response.success).toBeString();
        expect(response.success).toEqual('user_delete');
    });
});
