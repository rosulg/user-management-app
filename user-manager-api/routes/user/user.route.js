const Joi = require('joi');
const UserController = require('../../controllers/user.controller');

module.exports = [
    {
        method: 'GET',
        path: '/users',
        handler: async (request, h) => UserController.findAll(request, h),
        options: {
            description: 'Retrives a list of users',
            notes: "If query parameter 'page' is specified then the endpoint will return a list of users limited by 5",
            tags: ['api'],
            validate: {
                query: {
                    page: Joi.number().optional()
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/{id}',
        handler: async (request, h) => UserController.find(request, h),
        options: {
            description: 'Retrieves specified user',
            tags: ['api'],
            validate: {
                params: {
                    id: Joi.required()
                }
            },
            auth: 'default'
        }
    },

    {
        method: 'POST',
        path: '/user',
        handler: async (request, h) => UserController.create(request, h),
        options: {
            description: 'Creates a user and returns JWT',
            tags: ['api'],
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    password: Joi.string().min(8).required()
                }
            }
        }
    },

    /*
        Register is essentially the same process as /user ( create op. ).
        I left it to a stand alone route since the assigntment requires that
        the process of adding new users is the same as register but with the difference
        that it can be done only by authenticated users
    */
    {
        method: 'POST',
        path: '/register',
        handler: async (request, h) => UserController.create(request, h),
        options: {
            description: 'Creates a user',
            notes: 'Registers a new users. Returns a JWT',
            tags: ['api'],
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    password: Joi.string().min(8).required()
                }
            },
            auth: false
        }
    },

    {
        method: 'POST',
        path: '/login',
        handler: async (request, h) => UserController.login(request, h),
        options: {
            description: 'Signs the user in and returns a JWT',
            tags: ['api'],
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    password: Joi.string().min(8).required()
                }
            },
            auth: false
        }
    },

    {
        method: 'DELETE',
        path: '/user/{id}',
        handler: async (request, h) => UserController.remove(request, h),
        options: {
            description: 'Deletes specified user and related user actions',
            tags: ['api'],
            validate: {
                params: {
                    id: Joi.required()
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/validateUser/{encodedUserId}',
        handler: async (request, h) => UserController.validateUser(request, h),
        options: {
            description: 'Marks specified user as validated.',
            tags: ['api'],
            validate: {
                params: {
                    encodedUserId: Joi.required()
                }
            },
            auth: false
        }
    },

    {
        method: 'GET',
        path: '/resetPassword/{encodedUserId}',
        handler: async (request, h) => UserController.resetPassword(request, h),
        options: {
            description: "Resets specified users's password and sends an email to the user containing the new password",
            tags: ['api'],
            validate: {
                params: {
                    encodedUserId: Joi.required()
                }
            },
            auth: false
        }
    },

    {
        method: 'PUT',
        path: '/user/{id}',
        handler: async (request, h) => UserController.update(request, h),
        options: {
            description: "Updates specified user's email and password",
            tags: ['api'],
            validate: {
                params: {
                    id: Joi.required()
                },
                payload: {
                    email: Joi.string().email().required(),
                    password: Joi.string().min(8).required()
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/requestNewPassword',
        handler: async (request, h) => UserController.requestNewPassword(request, h),
        options: {
            description: 'Allows user to request for a new password. Sends an email with a confirmation link',
            tags: ['api'],
            validate: {
                payload: {
                    email: Joi.string().email().required()
                }
            },
            auth: false
        }
    }
];
