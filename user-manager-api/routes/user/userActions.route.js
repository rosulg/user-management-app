const Joi = require('joi');
const UserActionsController = require('../../controllers/userActions.controller');

module.exports = [{
    method: 'GET',
    path: '/userAction/{userId}/{actionType}',
    handler: request => UserActionsController.findAll(request),
    options: {
        description: 'Finds and returns all user actions for a user that matches the specified action type',
        tags: ['api'],
        validate: {
            params: {
                userId: Joi.string().required(),
                actionType: Joi.string().valid('login').required()
            }
        },
        auth: 'default'
    }
}];
