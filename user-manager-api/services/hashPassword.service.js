

const Bcrypt = require('bcrypt');
const Config = require('config');

module.exports = async function (next) {
    let user = (this.op === 'update') ? this._update.$set : this;
    user = (this.op === 'findOneAndUpdate') ? this._update : this;

    if (!user || !user.password || user.password.length === 60) {
        return next();
    }
    try {
        const salt = await Bcrypt.genSalt(Config.get('server.auth.saltFactor'));
        const hash = await Bcrypt.hash(user.password, salt);
        user.password = hash;

        return next();
    }
    catch (err) {
        return next(err);
    }
};
