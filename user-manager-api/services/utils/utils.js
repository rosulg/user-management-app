const Path = require('path');
const Mongoose = require('mongoose');

Mongoose.Promise = global.Promise;
const Config = require('config');


module.exports = {

    getFiles (filePath) {
        filePath = filePath[filePath.length - 1] !== '/' ? `${filePath}/` : filePath;
        let files = [];
        try {
            files = require('fs').readdirSync(Path.resolve(__dirname, '../..', filePath)); // eslint-disable-line global-require
        }
        catch (e) {
            console.log(e);
            process.exit();
        }
        return files.map((file) => {
            return Path.resolve(__dirname, '../..', filePath, file);
        });
    },

    addRoute (server, routes) {
        this.getFiles(routes || 'routes').forEach((routesFile) => {
            if (routesFile.includes('.route.js')) {

                require(routesFile).forEach((route) => { // eslint-disable-line import/no-dynamic-require
                    server.route(route);
                });
            }
            else {
                const routeDirectory = routesFile;
                this.addRoute(server, routeDirectory);
            }
        });
    },

    async addPolicies (server) {
        await server.register(require('hapi-auth-jwt2')); // eslint-disable-line global-require

        this.getFiles('policies').forEach((policyFile) => {
            /* eslint-disable-line global-require  */ const policy = require(policyFile); // eslint-disable-line import/no-dynamic-require
            const name = Path.basename(policyFile, '.js');
            const namePolicie = name.split('.')[0];


            server.auth.strategy(namePolicie, 'jwt', {
                key: Config.get('server.auth.secretKey'),
                validate: policy,
                verifyOptions: {
                    algorithms: ['HS256']
                }
            });
        });
        server.auth.default('default', 'jwt');
    },

    addModels () {
        global.Models = {};

        this.getFiles('models').forEach((modelFile) => {
            /* eslint-disable-line global-require  */ const modelInterface = require(modelFile); // eslint-disable-line import/no-dynamic-require
            const schema = new Mongoose.Schema(modelInterface.schema, {
                versionKey: false
            });

            const name = Path.basename(modelFile, '.js');

            if (modelInterface.statics) {
                Object.keys(modelInterface.statics).forEach((modelStatic) => {
                    schema.statics[modelStatic] = modelInterface.statics[modelStatic];
                });
            }

            if (modelInterface.methods) {
                Object.keys(modelInterface.methods).forEach((modelMethod) => {
                    schema.methods[modelMethod] = modelInterface.methods[modelMethod];
                });
            }

            if (modelInterface.onSchema) {
                Object.keys(modelInterface.onSchema).forEach((type) => {
                    Object.keys(modelInterface.onSchema[type]).forEach((func) => {
                        if (Array.isArray(modelInterface.onSchema[type][func])) {
                            for (let i = 0; i < modelInterface.onSchema[type][func].length; i += 1) {
                                schema[type](func, modelInterface.onSchema[type][func][i]);
                            }
                        }
                        else {
                            schema[type](func, modelInterface.onSchema[type][func]);
                        }
                    });
                });
            }

            let nameModel = name.split('.')[0];
            nameModel = nameModel.charAt(0).toUpperCase() + nameModel.slice(1);
            Models[nameModel] = Mongoose.model(nameModel, schema);
        });
    }
};
