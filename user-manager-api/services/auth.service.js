const Config = require('config');
const jwt = require('jsonwebtoken');
const Bcrypt = require('bcrypt');

module.exports = {

    async comparePassword (candidatePassword, password) {
        try {
            const compare = await Bcrypt.compare(
                candidatePassword,
                password
            );
            return compare;
        }
        catch (error) {
            return false;
        }
    },

    async authenticate (user, providedPassword) {

        const isValidPassword = await this.comparePassword(
            providedPassword,
            user.password
        );

        if (isValidPassword) {
            const data = {
                id: user._id,
                token: jwt.sign({
                    id: user._id
                }, Config.get('server.auth.secretKey'))
            };

            return data;
        }
        return null;
    },

    async createToken (user) {
        return jwt.sign({
            id: user._id
        }, Config.get('server.auth.secretKey'));
    },

    verifyToken (token) {
        return jwt.verify(token, Config.get('server.auth.secretKey'));
    }
};
