const path = require('path');
const sgMail = require('@sendgrid/mail');
const Config = require('config');
const fileGenerator = require('./lib/fileGenerator');

sgMail.setApiKey(Config.get('server.emailService.apiKey'));

module.exports = {

    async loadBody (content, template) {
        const filePath = path.resolve(
            __dirname, `./lib/templates/${template}`,
        );
        const file = await fileGenerator.generateDocumentFromTemplate(filePath, content);
        return file;
    },

    async sendEmail (content, template) {
        try {
            const body = await this.loadBody(content, template);

            const emailUser = Config.get('server.emailService.user');

            const receiver = content.email;

            const message = {
                to: `<${receiver}>`,
                from: `User Management App <${emailUser}>`,
                subject: 'Message from User Management App',
                html: body
            };

            if (Config.get('server.emailService.enabled')) {
                await sgMail.send(message);
            }

            return {
                message: `Email sent to ${receiver}`,
                content
            };
        }
        catch (e) {
            console.log(e, 'email not sent');
            return null;
        }
    }
};
