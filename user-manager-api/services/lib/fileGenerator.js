const dot = require('dot');
const fs = require('fs');
const path = require('path');

module.exports = {
    generateDocumentFromTemplate (templatePath, content) {
        dot.templateSettings.strip = false;
        return new Promise((resolve, reject) => {
            try {
                const template = dot.template(fs.readFileSync(path.resolve(__dirname, templatePath)));
                resolve(template(content));
            }
            catch (e) {
                reject(e);
            }
        });
    },

    async generateDocument (filePath, templatePath, content) {
        const document = await this.generateDocumentFromTemplate(templatePath, content);
        await fs.appendFileSync(filePath, document, 'utf-8');
    },

    async deleteDocument (filePath) {
        await fs.unlinkSync(filePath);
    }
};
